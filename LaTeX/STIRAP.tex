\label{chap:stirap}

To further the scope of this report, one can move on to a system with yet another dimension. The STIRAP process (short for stimulated Raman adiabatic passage) deals with population transfers between two quantum states each coupled to another state. The states can be coupled in three different ways as illustrated in \cref{fig:layoutstirap} but the formation is irrelevant in this report, as the coupling amplitudes $\Omega_1$ and $\Omega_2$ do not depend on direction (\cite{3levelsnote}).

\begin{figure}
	\centerfloat
	\includegraphics[width = 1.3\linewidth]{../Andet/stiraplayout.png}
	\caption{In the STIRAP system, the three energy states can be coupled in three different formations leading to the ladder- (a), the $\Lambda$- (b) and the V-systems. $\Omega_1$ and $\Omega_2$ are the amplitudes of the couplings between the states. This figure is inspired by the first figure in \cite{3levelsnote}.}
	\label{fig:layoutstirap}
\end{figure}

\section{Theory}
\label{sec:stiraptheory}

The STIRAP process allows total transfer of population from state $|1\rangle$ to the target state $|3\rangle$. Though it seems as though the populations should go through state $|2\rangle$, this is not the case in the adiabatic limit (\cite{nogetmedrydberg}). By modifying the Rabi frequencies, $\Omega_1$ and $\Omega_2$, in the correct manner, the middle state will be avoided completely, and the population will flow directly from $|1\rangle$ to $|3\rangle$. The manipulation of the coupling is counter-intuitive, meaning that if the population begins in $|1\rangle$, the field between states $|2\rangle$ and $|3\rangle$ is activated first. Then the other field is increased as the first is turned off. This means, that $\Omega_2$ is activated first, and $\Omega_1$ is activated last, despite what one intuitively should believe (\cite{3levelsnote}). \\
Naturally, the process is described by \cref{eq:timedepschrodinger}, and the time-dependent Hamiltonian is in this case written as
\begin{align}
	H = 
	\begin{pmatrix}
	0 & \Omega_1(t) & 0 \\
	\Omega_1(t) & 0 & \Omega_2(t) \\
	0 & \Omega_2(t) & 0
	\end{pmatrix} \, .
\end{align}
This is described in the basis made up of three states called \textit{dark}, \textit{excited}, and \textit{bright}. They are:
\begin{align}
	|\psi_{dark}\rangle &= A \left(\Omega_2|1\rangle - \Omega_1|3\rangle \right)\, ,\nonumber\\
	|\psi_{excited}\rangle &= |2\rangle\, , \\
	|\psi_{bright}\rangle &= A\left( \Omega_1|1\rangle + \Omega_2|3\rangle \right)\, .  \nonumber
\end{align}
where $A = \left(\sqrt{\Omega_1^2+\Omega_2^2}\right)^{-1}$ is the normalisation factor.\\
This yields a total of three eigenstates of the Hamiltonian, where $\psi_{dark}$ is one of them with eigenvalue 0. $\psi_{excited}$ and $\psi_{bright}$ make up a coupled system of degenerate states, making the remaining eigenstates $\psi_\pm = 2^{-1/2}\left(\psi_{bright}\pm\psi_{excited}\right)$. Their eigenvalues are $\pm \sqrt{\Omega_1^2+\Omega_2^2}$. All eigenvalues are depicted in \cref{fig:stirapeigvals}. This report deals with the eigenstate $\psi_{dark}$ with eigenvalue 0.
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../Kode/stirap/Egenvaerdier.pdf}
	\caption{The eigenvalues of the STIRAP system.}
	\label{fig:stirapeigvals}
\end{figure}

\subsection{Bloch illustration}
\label{subsec:blochstirap}

As described in \cref{sec:bloch}, the Bloch description can be used to illustrate the states of a two-level system. However, the STIRAP system is a \textit{three}-level system. To circumvent this problem, the differential Bloch equations are written
\begin{align}
	\dot{\mathbf{R}} = \mathbf{R}\times\mathbf{W}\, ,
	\label{eq:blochdiff}
\end{align}
where $\mathbf{R} = \left[u,v,w\right]$ is the Bloch vector, $\dot{\mathbf{R}}$ is its time derivative and $\mathbf{W}=\left[\Omega,0,\delta\right]$ is the torque vector. In the two-state Bloch equations, $\Omega$ is the Rabi frequency, and $\delta$ is the detuning (\cite{amo}).\\
A very similar equation can be written for the STIRAP system. Here $\mathbf{B}$ is used for the Bloch vector and $\mathbf{Q}$ is used for the torque vector. They are defined as:
\begin{align}
	\mathbf{B} = \left[u,v,w\right] \quad , \quad \mathbf{Q} = \left[-\Omega_1,0,\Omega_2\right]^T\, .
\end{align}
Thus, the following equation can be written:
\begin{align}
	\dot{\mathbf{B}} = \mathbf{Q}\times\mathbf{B} \, .
\end{align}
This is very similar to \cref{eq:blochdiff}. The components of $\mathbf{B}$ are, however, very different from $\mathbf{R}$. They are given
\begin{align}
	u(t) = -c_3(t) \quad , \quad v(t) = -ic_2(t) \quad , \quad w(t) = c_1(t) \, .
\end{align}
where $c_i$ are the coefficients of the wave function that solves \cref{eq:timedepschrodinger}. This renaming is not very intuitive but it works (\cite{STIRAP}).\\
With all this in order, the Bloch sphere can be illustrated as seen in \cref{fig:stirapbloch}. Here, it is seen, that the systems starts out with all the population in state $|1\rangle$, since the arrow at $t=0$ (the beginning of the red line) points at the northern pole, such that $w(0) = 1$ and $u(0) = v(0) = 0$. The blue arrow indicates the state at $t=T$, which is a point at the equator such that $|u(T)| = 1$ and $v(T)=w(T)=0$. 
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../Kode/stirap/Bloch.pdf}
	\caption{The change from state $|1\rangle$ to state $|3\rangle$ is shown by the red line in the Bloch sphere. The blue arrow points to the end point on the equator, where all population has changed from the first state to the last.}
	\label{fig:stirapbloch}
\end{figure}

\subsection{Designing $\Omega$}
\label{subsec:omega}
Similarly to $\theta$ in \cref{subsec:theta}, the coupling amplitudes $\Omega_1$ and $\Omega_2$ need to be designed properly. As argued earlier, non-differentiable points are problematic, and if simple sine-functions are used, we achieve the dashed line in \cref{fig:omega}, that definitely shows this unwanted behaviour.\\
Instead, one could simply use a sine to a higher power, however, this creates a new problem. $\sin^5$ or $\sin^{10}$ become very narrow compared to $\sin^1$, and thus the coupling only holds for a short duration.\\
To solve this complication, one can combine the two methods. The beginning and end will follow a sine raised to the power of $n$, while the middle follows a regular sine. The joining point of the two functions depends on the power of the sine, and it is solved and joined numerically. In \cref{fig:omega}, this is shown for $\sin^3$ as the blue and red curves.
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../Kode/stirap/Omegas.pdf}
	\caption{Two possibilities for the coupling amplitudes. The red and blue lines are $\Omega_1$ and $\Omega_2$, respectively. They have rounded edges following $\sin^3$ in the low values of $\Omega_i(t)$ and changing to $\sin^1$ for the middle part. The dashed black lines show $\sin^1$ all the way. These clearly have shard edges along the $x$-axis, and they are wider.}
	\label{fig:omega}
\end{figure}


\section{Results}
Firstly, an analytical solution was determined, and it is depicted in \cref{fig:stirapanalytical}. Then, there are two properties that influence the fidelity and these can be varied using the developed Python script. These are the total time and the power to which the sine is raised when designing the coupling amplitudes as described in \cref{subsec:omega}. 
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../Kode/stirap/analyticalSolution.pdf}
	\caption{The analytical solution to the time-dependent Schrödinger equation. The populations of the three states are seen and it is evident, that the population of state $|2\rangle$ should be 0 throughout the process while the two other states switch.}
	\label{fig:stirapanalytical}
\end{figure}
\subsection{Time}
The total time, $T$, determines how fast or slow the process takes place. For low values of $T$, the population of $|1\rangle$ does not completely transfer to another state. For sufficiently low values, it transfers partly to $|2\rangle$ and not at all to state $|3\rangle$ which is the opposite of what the STIRAP process aims to do. For increasingly higher values of $T$, state $|3\rangle$ receives more population through the process, while $|2\rangle$ is entirely unpopulated throughout the entire process. \\
This is all seen in \cref{fig:stiraptid}. Here, the time axes show $t/T$, so it is easier to compare the developments of the graphs. Furthermore, the restoration of fidelity is due to the coupling amplitudes that follow $\sin^3$ in both ends and $\sin^1$ in the middle.\\
In this figure, six solutions with vastly different time scales are shown on the left-hand side, while the corresponding fidelities are seen on the right-hand side. Here, it is evident, that the fidelity is extremely small for small values of $T$ while it becomes much better with the high values. However, the phenomenon of the regaining of fidelity is seen even for low values of $T$. \\
For even higher values of $T$, one can look at \cref{fig:stiraptidmegetstort}, that has the same axes and $\Omega$s as the former figure. It shows all time frames regain fidelity so the end values of $F$ are almost 1. This behaviour is expected in the adiabatic limit.\\
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../Kode/stirap/korttid.pdf}
	\caption{The left side shows six numerical solutions to the Schrödinger equation where the total time varies from low to high numbers. The right side shows the corresponding fidelities. On the $x$-axis, $t/T$ is shown for easier comparison. This figure includes values of $T$ that both adhere to the adiabatic approximation and some that do not.}
	\label{fig:stiraptid}
\end{figure}
\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{../Kode/stirap/langtid.pdf}
	\caption{The left side shows six numerical solutions to the Schrödinger equation where the total time varies from low to high numbers. The right side shows the corresponding fidelities. On the $x$-axis, $t/T$ is shown for easier comparison. This figure includes very large values of $T$ such that the adiabatic approximation holds for all six solutions.}
	\label{fig:stiraptidmegetstort}
\end{figure}

\subsection{The power of the sine}
Another key factor is the power to which the sine is raised in the coupling amplitudes, $\Omega_1$ and $\Omega_2$. The higher the power, the more smooth the $\Omega$s are.\\ 
In \cref{fig:stirappotens}, the total time is set to $T=500$ and six solutions and their corresponding fidelities in the adiabatic limit are found. It is seen that the higher the power, the bigger the error is during the evolution, but it also decreases the final error considerably.\\ 
It is clear, that the worst solution is $\sin^1$ because of its edged coupling amplitudes, but $\sin^6$ is the second worst solution. This is due to the fact that for the higher powers, the coupling is shorter as mentioned in \cref{subsec:omega}. The coupling lasting for a shorter duration is also clear from the depicted solutions in\cref{fig:stirappotens}. The middle part of $\sin^6$ is much steeper than that of $\sin^2$. \\
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../Kode/stirap/potenserLangtid.pdf}
	\caption{The left part of the figure contains six numerical solutions to the Schrödinger equations with differing coupling amplitudes. The coupling amplitudes follow a sine in the beginning and the end and this sine is raised to a variable power. Their individual fidelities are plotted on the right side, where clear oscillations are seen.}
	\label{fig:stirappotens}
\end{figure}

