\label{chap:spin}

The first case handled in this report is a simple particle with two available energy eigenstates - spin up, $|\uparrow\,\rangle$, and spin down, $|\downarrow \, \rangle$. It is placed in a time-dependent magnetic field, $\mathbf{B}$. This is referred to as the spin system (\cite{kvant}).

\section{Theory}
\label{sec:spintheory}

The particle has two spin states governed by the spin vector, $\mathbf{S}$. Its three coordinates are given
\begin{align}
\mathbf{S}_x = \frac{\hbar}{2}\begin{pmatrix} 0 & 1 \\ 1 & 0 \end{pmatrix}\quad, \quad \mathbf{S}_y = \frac{\hbar}{2} \begin{pmatrix} 0 & -i \\ i & 0	\end{pmatrix}\quad, \quad \mathbf{S}_z = \frac{\hbar}{2} \begin{pmatrix} 1 & 0\\ 0 & -1 \end{pmatrix} \, .
\label{eq:spin}
\end{align}
The particle is then placed in an external magnetic field, $\mathbf{B}$, that can be described by the spherical coordinates
\begin{align}
	\mathbf{B} = B_0\left( \sin{\theta}\cos{\phi}, \sin{\phi}\cos{\phi}, \cos{\theta} \right)\, ,
	\label{eq:Bfield}
\end{align}
where $B_0$ is the strength of the magnetic field.\\
The Hamiltonian of a spinning particle in a magnetic field is
\begin{align}
	H = -\gamma\,\mathbf{B}\cdot\mathbf{S} \, ,
	\label{eq:Hamiltonian}
\end{align}
where $\gamma$ is the gyromagnetic ratio.\\
The particle starts out in one state and can change to the other. This can be imagined as an arrow tracing a path from one pole to the other on a sphere (see \cref{subsec:blochspin}). This only requires half an evolution of the polar angle and no change in the azimuthal angle. This is expressed as
\begin{align}
	\phi = 0 \quad , \quad \theta \in [0,\pi] \, .
	\label{eq:angles}
\end{align}
Above, the time-dependency of $\mathbf{B}$ is mentioned, and it is now clear, that this dependency must lie in $\theta$. To achieve adiabaticity, $\theta$ must be manipulated - this is covered in more detail in \cref{subsec:theta}.\\
Using \cref{eq:angles}, the Hamiltonian in \cref{eq:Hamiltonian} then becomes
\begin{align}
	H = \frac{\hbar B_0}{2}\begin{pmatrix}
		\cos{\theta} & \sin{\theta} \\ \sin{\theta} & -\cos{\theta}
	\end{pmatrix} \, .
\end{align}
Naturally, this is in the basis consisting of $|\uparrow\,\rangle$ and $|\downarrow\,\rangle$. Diagonalising the Hamiltonian leads to eigenvalues 1 and -1, as seen in \cref{fig:spineigvals}.
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../Kode/spin/Egenvaerdier.pdf}
	\caption{The eigenvalues of the spin system.}
	\label{fig:spineigvals}
\end{figure}

\subsection{Bloch illustration}
\label{subsec:blochspin}

Using the description of Bloch theory in \cref{sec:bloch}, the Bloch sphere of this system can be created. It is illustrated in \cref{fig:spinbloch}. Here it is seen, that the system starts out at $t=0$ in the state where $w(0) = 1$ and $u(0) = v(0) = 0$. As time passes, it rotates and ends at $t=T$, where $w(T) = -1$ and $u(T) = v(T) = 0$. This means, that the population has been transferred completely from state $|\uparrow\,\rangle$ to state $|\downarrow\,\rangle$.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../Kode/spin/Bloch.pdf}
	\caption{The change from state $|\uparrow\,\rangle$ to state $|\downarrow\,\rangle$ is shown in the Bloch sphere. The red line shows the path and the blue arrow points to the end location. It is clear, that the restrictions in \cref{eq:angles} are upheld.}
	\label{fig:spinbloch}
\end{figure}

\subsection{Designing $\theta$}
\label{subsec:theta} 

The evolution of the angle, $\theta$, dictates the speed with which the system develops. In the most basic case, $\theta$ is linear with values 0 and 1 at $t=0$ and $t = T$, respectively. This is shown as a blue graph in \cref{fig:theta}. On the one hand, the gradient is low. On the other, the curve shows two clear breaks where the function is non-differentiable - namely where $\theta$ changes between constant and linear. These edges are problematic as they result in a large fidelity error, so the curve needs to be smoothed out.\\
Doing this is a somewhat complicated matter, since the smoother the beginning and end become, the steeper the linear section in the middle gets. The steepness is also a problem, since the process thus occurs quicker, so designing $\theta$ becomes a matter of optimisation. In \cref{fig:theta}, the red, dashed line shows such a smooth curve. This curve is proportional to $t^5$ in the beginning and end, while it is simply linear in the middle part. \\
\begin{figure}
	\centering 
	\includegraphics[width=\linewidth]{../Kode/spin/Theta.pdf}
	\caption{Two variations of $\theta$ is shown. The solid blue line illustrates a purely linear model with sharp edges at $t=0$ and $t=T$. The dashed red line shows a curve with rounded edges, however, it has a steeper middle part.}
	\label{fig:theta}
\end{figure}

\section{Results}
While diagonalising the Hamiltonian and finding its eigenstates was done analytically, solving the Schrödinger equation and measuring the fidelity was done numerically in the developed Python script. The analytical solution is illustrated in \cref{fig:spinanalytical} where the populations of the two states are seen.\\
To investigate the system, two different properties are varied - the total time, $T$, and the power to which $t$ is raised (as described in \cref{subsec:theta}). These values were changed in the appropriate places in the script.
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../Kode/spin/analyticalSolution.pdf}
	\caption{The analytical solution to the time-dependent Schrödinger equation. Here it is seen, that as the population of state $|\uparrow\rangle$ (the red line denoted $|c_1|^2$) decreases while the population of state $|\downarrow\rangle$ (the blue line denoted $|c_2|^2$) increases. In the analytical solution they go from exactly 1 and 0 to exactly 0 and 1, respectively.}
	\label{fig:spinanalytical}
\end{figure}
\subsection{Time}
The total time of the system is $T=\pi/a$ where $a$ is a variable number. It is clear, that with smaller $a$-values, $T$ must increase accordingly, leading to a slower change in time. With a sufficiently small $a$, the adiabatic limit will be approached. \\
In \cref{fig:spintid}, this process of comparing the numerically found adiabatic solution to the analytically found one is repeated for six different values of $a$. To make comparison easier, the  time axes are not $t$, but $t/T$, so the $t/T$-values range between 1 and 0. Furthermore, all solutions have a $\theta$ with rounded edges, such that it is proportional to $t^3$ in both ends.\\
On the left part of the figure, the six numerical solutions are found, while the corresponding fidelities are shown in the larger right-hand figure. For the high values of $a$, the fidelity is very low meaning the population has hardly shifted from the first state to the next. \\
However, even for $a=1$, an interesting phenomenon is shown. The fidelity drops very low during the process, but then increases in the end. This is exactly what was described in \cref{chap:intro}. \\
This behaviour is even more dominant for smaller values of $a$, though the "huge" deviation in the middle is still very small compared to the error of $a=5$. In \cref{fig:spintidlillea}, the same procedure is repeated, however, the $a$-values are all small enough to approach the adiabatic limit. All solutions match the analytical one very well making the error appear on the second or third digit.\\
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../Kode/spin/varieretA.pdf}
	\caption{On the left-hand side, six numerical solutions to the Schrödinger equation is found. The red line is the population of $|\uparrow\,\rangle$ and the blue line is the population of $|\downarrow\rangle$. On the right-hand side, the comparison between the numerical solutions and the analytical one is shown as fidelities. On the $x$-axis, $t/T$ is shown to make comparison easier despite the variable time scales. This figure contains varied values of $a$ such that both the adiabatic and non-adiabatic solutions are seen.}
	\label{fig:spintid}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../Kode/spin/megetlillea.pdf}
	\caption{On the left-hand side, six numerical solutions to the Schrödinger equation is found. The red line is the population of $|\uparrow\,\rangle$ and the blue line is the population of $|\downarrow\rangle$. On the right-hand side, the comparison between the numerical solutions and the analytical one is shown as fidelities. On the $x$-axis, $t/T$ is shown to make comparison easier despite the variable time scales. This figure contains very small values of $a$ such that the adiabatic approximation holds for all six solutions.}
	\label{fig:spintidlillea}
\end{figure}

\subsection{The power of $t$}
Another property worth investigating is the power to which $t$ is raised. Choosing a large time scale with $a=0.01$, and thus letting all solutions approach the adiabatic limit, makes it easy to see the power's influence on the fidelity.\\
In \cref{fig:spinpotens}, six different values are shown, where the left part of the figure shows the solutions themselves, and the right part shows the fidelities of the six cases. \\
It is apparent, that the edged solution, $t^1$ shows a behaviour different to the other solutions. It oscillates wildly from the beginning with a nearly constant amplitude and frequency. The other solutions show a clear drop in fidelity in the middle of the perturbation, but it is restored near the end. This is due to the rounded edged in $\theta$. \\
The left part of the figure clearly shows, that for the higher powers, the middle part of the solutions are extremely steep, just as expected from \cref{subsec:theta}.
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{../Kode/spin/potenseramegetlille.pdf}
	\caption{The left side shows six numerical solutions to the Schrödinger equation, where the red and blue lines represent the populations of states $|\uparrow\,\rangle$ and $|\downarrow\,\rangle$ respectively. The right side shows the fidelities of the adiabatic solutions compared to the analytical solution.}
	\label{fig:spinpotens}
\end{figure}
