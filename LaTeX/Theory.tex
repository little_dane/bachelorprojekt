\section{The Schrödinger equation}
\label{sec:schrodinger}

As in most quantum mechanics, this report relies heavily on the Schrödinger equation - or more specifically, the time-dependent version of it. It is expressed as
\begin{align}
i\hbar\frac{\partial}{\partial t}|\Psi(t)\rangle = H(t)|\Psi(t)\rangle \, .
\label{eq:timedepschrodinger}
\end{align}
Any solution, $\Psi(t)$ , can be written as a linear combination of the basis elements. In this report, \cref{eq:timedepschrodinger} is solved in a fixed basis consisting of the energy eigenstates (\cite{kvant}). For instance, in \cref{chap:spin}, the basis is formed by the two spin states - up and down. 

\section{Adiabaticity in quantum mechanics}
\label{sec:adiab}
When working with quantum mechanics, adiabaticity does not concern processes that occur without a transfer of heat as it is in thermodynamics. Instead, it governs processes that involve gradual changes of the Hamiltonian. This way, the system can keep up with the changes and modify itself to the new conditions (\cite{kvant}).\\


\subsection{The adiabatic theorem}
The adiabatic theorem describes the eigenstates of a time-dependent Hamiltonian, $H(t)$. At $t=0$, it has the value $H_i$, while it has value $H_f$ at time $t=T$. The change in the Hamiltonian can occur in one of three ways - slow, quick, or intermediate. This speed will affect a particle in the $n$th eigenstate of $H_i$. \\
If the change is slow, the adiabatic theorem states that the particle in question will remain in the $n$th eigenstate through every infinitesimal change and end in the $n$th eigenstate of $H_f$ (\cite{kvant}).\\
However, if the perturbation occurs rapidly, the system will not have enough time to react, and the particle will remain in the $n$th eigenstate of $H_i$ instead of changing. \\
The last possibility is the change happening at an intermediate speed, which will result in an oscillating eigenstate trying to keep up with the changing Hamiltonian.\\
In this report, these claims are shown numerically, and it is seen that the speed of the change has a significant impact on the fidelity (\cref{sec:fidelity}).


\subsection{The adiabatic phase factors}
\label{subsec:proof}
The time-dependent Schrödinger equation is briefly covered in \cref{sec:schrodinger}, however, the adiabatic solution to \cref{eq:timedepschrodinger} contains several interesting phase factors, so this will be covered in the following. This proof closely resembles that of chapter 10 in \cite{kvant}. \\
Just as in the non-adiabatic case, \cref{eq:timedepschrodinger} must be solved. In this case, however, the Hamiltonian is time-dependent, meaning that all eigenstates and -values are time-dependent as well. This makes it difficult to create a fixed basis of the energy eigenstates, so it is done as a "snapshot". At a given time $t$, the Hamiltonian will have value $H(t)$ and eigenstates $\psi_n(t)$. This means that at any given instant a fixed basis can be formed of the instantaneous energy eigenstates. \\
Thus a solution to \cref{eq:timedepschrodinger} can be written as
\begin{align}
\Psi (t) = \sum_{n} c_n(t)\psi_n(t)e^{i\theta_n(t)} \quad , \quad \theta_n(t) = -\frac{1}{\hbar}\int_{0}^{t} E_n(t')dt' \, ,
\label{eq:adiabsolutionschrodinger}
\end{align}
where $\theta_n(t)$ is called the dynamic phase factor and consists of an integral of the energies, as they are time-dependent in this case.\\
By plugging \cref{eq:adiabsolutionschrodinger} into \cref{eq:timedepschrodinger}, one gets
\begin{align}
i\hbar|\dot{\Psi}(t)\rangle &= H(t)|\Psi(t)\rangle\, .
\end{align}
where the dot denotes the time derivative. This can easily be reduced to
\begin{align}
\sum_n \dot{c}_n(t)\psi_n(t)e^{i\theta_n(t)} = -\sum_n c_n(t)\dot{\psi}_n(t)e^{i\theta_n(t)} \, .
\end{align}
Now, by using the above to compare two instantaneous eigenstates $\psi_n(t)$ and $\psi_m(t)$ taking the inner product it becomes
\begin{align}
\sum_n \dot{c}_n(t)\langle\psi_m(t)|\psi_n(t)\rangle e^{i\theta_n(t)} = -\sum_n c_n(t)\langle\psi_m(t)|\dot{\psi}_n(t)\rangle e^{i\theta_n(t)} \, .
\end{align}
Using the fact, that the instantaneous eigenstates form a complete orthogonal basis, a set of coupled differential equations describing the coefficients appear,
\begin{align}
\dot{c}_m(t) &= -\sum_{n}c_n\langle\psi_m|\dot{\psi}_n\rangle e^{i(\theta_n-\theta_m)}\nonumber\\
&= -c_m\langle\psi_m|\dot{\psi}_m\rangle-\sum_{n\neq m}c_n\frac{\langle\psi_m|\dot{H}|\psi_n\rangle}{E_n-E_m}e^{(-i/\hbar)\int_{0}^{t}[E_n(t')-E_m(t')]dt'} \, .
\label{eq:coefficientslong}
\end{align}
These are the time-dependent coefficients of the wave functions under the assumption that there are no degenerate energies.\\
To simplify this, the adiabatic approximation is used. Assuming the change occurs very slowly, $\dot{H}$ becomes very small.
It is not trivial why this justifies the second term to be neglected, since $\dot{\psi}_n$  in the first term will also be small when $\dot{H}$ is. However, the oscillating term in the exponential function will with a larger time frame go far quicker to zero than any of the time derivatives. \\
This leaves a differential equation with the solution
\begin{align}
c_m(t) = c_m(0)e^{i\gamma_m(t)} \quad , \quad
\gamma_m(t) = i\int_{0}^{t}\langle\psi_m(t')|\dot{\psi}_m(t')\rangle dt' \, .
\label{eq:coeffdiff}
\end{align} 
$\gamma_m(t)$ is called the geometric phase or Berry's phase. Without the adiabatic approximation, the second term in \cref{eq:coefficientslong} would be very far from negligible. Since amplitudes are the squares of the populations, the error would be quadratic. \\
In the end, this means, that if the solution to the Schrödinger equation, $\Psi$, starts in one of the instantaneous eigenstates, $\psi_n$, it will at any given time remain in this state, only having gained two phase factors,
\begin{align}
\Psi(0) = \psi_n(0) \quad \Rightarrow \quad \Psi(t) = e^{i\theta_n(t)} e^{i\gamma_n(t)} \psi_n(t) \, .
\end{align}


\section{Fidelity}
\label{sec:fidelity}
Fidelity is a measure of how much overlap there is between wave functions. It is given
\begin{align}
F(\psi_1,\psi_2) = \left| \langle \psi_1 | \psi_2 \rangle \right|^2 \, .
\label{eq:fidelity} 
\end{align}
This will yield a value between 1 and 0, where $F=1$ means that $\psi_1$ and $\psi_2$ fully overlap, while $F=0$ means they do not overlap whatsoever. When speaking of a good fidelity, it means that $F$ is close to 1.\\ 
In this report, two wave functions are found and compared to each other. One is determined by diagonalising the Hamiltonian and determining the eigenstates and -values. This leads to the analytical solution, $\Psi_A$. The other solution is found numerically using the developed Python script. This one is called $\Psi_N$ and is computed by solving the time-dependent Schrödinger equation (\cref{eq:timedepschrodinger}).\\
To find out, how well the two solutions match, the entity $F(\Psi_N,\Psi_A)$ is calculated and plotted. This gives an excellent depiction of how far the adiabatic solution, $\Psi_N$ is from the "regular" solution, $\Psi_A$.

\section{Bloch description}
\label{sec:bloch}
To visualise quantum states, one can use the Bloch sphere and the associated Bloch vectors. In this report, the Bloch description is used to illustrate the eigenstates of the spin and STIRAP systems and will be described more in-depth in sections \ref{subsec:blochspin} and \ref{subsec:blochstirap}, respectively. \\
Generally, Bloch theory is based on the Bloch vector, $\mathbf{R} = u\hat{\mathbf{e}}_1+v\hat{\mathbf{e}}_2+w\hat{\mathbf{e}}_3$ where $\hat{\mathbf{e}}_i$ are the unit vectors of the system. The coefficients $u$, $v$ and $w$ are defined by the coefficients of the wave function in question
\begin{align}
u &= 2\;\text{Re}\left(c_1c_2^*\right) \, ,\nonumber \\
v &= 2\;\text{Im}\left(c_1c_2^*\right) \, , \label{eq:bloch} \\
w &= \left|c_1\right|^2-\left|c_2\right|^2 \, . \nonumber
\end{align}
$\mathbf{R}$ can then be used to illustrate the states of the system. For instance, if the system is in state $|1\rangle$, the Bloch vector becomes $\mathbf{R}=w\hat{\mathbf{e}}_3$. In the Bloch sphere, this means, that $\mathbf{R}$ points directly to the northern pole. Likewise, $\mathbf{R}$ can point to any point on the surface of the Bloch sphere indicating the corresponding state (\cite{amo}).\\
