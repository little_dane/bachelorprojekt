\babel@toc {danish}{}
\babel@toc {english}{}
\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Summary}{i}{Doc-Start}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Resum\IeC {\'e}}{ii}{section*.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Acknowledgements}{iii}{section*.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {1}Introduction}{1}{chapter.1}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {2}Theory}{2}{chapter.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}The Schr\IeC {\"o}dinger equation}{2}{section.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Adiabaticity in quantum mechanics}{2}{section.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.1}The adiabatic theorem}{2}{subsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.2}The adiabatic phase factors}{3}{subsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Fidelity}{5}{section.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Bloch description}{5}{section.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {3}Spin system}{7}{chapter.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Theory}{7}{section.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}Bloch illustration}{8}{subsection.3.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}Designing $\theta $}{8}{subsection.3.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Results}{10}{section.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.1}Time}{10}{subsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2.2}The power of $t$}{14}{subsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {4}STIRAP}{16}{chapter.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.1}Theory}{16}{section.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.1}Bloch illustration}{17}{subsection.4.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1.2}Designing $\Omega $}{19}{subsection.4.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4.2}Results}{20}{section.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.1}Time}{21}{subsection.4.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2.2}The power of the sine}{23}{subsection.4.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {5}Discussion}{25}{chapter.5}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {6}Conclusion}{28}{chapter.6}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Bibliography}{29}{section*.20}
