import scipy.integrate
import numpy as np
import matplotlib.pyplot as plt
import cmath
from omegaberegner import make_omega

################FUNKTIONER#################

def H(t):
    H = np.array([[0, omega1(t), 0], [omega1(t), 0, omega2(t)], [0, omega2(t), 0]],dtype=complex)
    return H

def dPsidt(t,Psi):
    dPsidt = 1/(1j*h_bar)*np.dot(H(t),Psi)
    return dPsidt
"""
def omega2(t):
	if t <= 0:
		return 0
	elif t >= np.pi*N:
		return 0
	else:
		return np.sin(t/N)**M

def omega1(t):
	if t <= N:
		return 0
	elif t >= T:
		return 0
	else:
		return np.sin((t-N)/N)**M
"""
###########LØSNINGEN AF SCHRÖDINGERS LIGNING###################

#Definition af variable
h_bar = 1.
Psi_start = np.array([1,0,0],dtype=complex) #Måske en anden starttilstand

M = 2. #Sinus' potens
T = 5 
t = np.linspace(0,T,10000)
t_range =[t[0],t[-1]]
offset = 1.5
omega1, omega2 = make_omega(t_range,offset,M)


#For at få complex_ode til at virke, skal man sætte konstanten først
diff_eq = lambda t,y: dPsidt(t,y)

#Nu kan complex_ode kaldes og Hamiltonen udvikles
Psi_solve = scipy.integrate.complex_ode(dPsidt)
Psi_solve.set_initial_value(Psi_start,t[0])
Psi_solve.set_integrator('dop853')

#Først tilføjes det 0'te element, fordi dette udregnes før for-loopet
c = np.zeros((len(t),3),dtype=complex)
c[0,0] = Psi_start[0]
c[0,1] = Psi_start[1]
c[0,2] = Psi_start[2]


#Nu samles resterende data i listerne
for i,t_int in enumerate(t[1:]):
    out = Psi_solve.integrate(t_int)
    c[i+1,0] = out[0]
    c[i+1,1] = out[1]
    c[i+1,2] = out[2]

#Den udviklede bølgefunktion
Psi_udv = c

#Nedenstående vektor består af normkvadraterne af c1 og c2
Psi_udv_normkv = np.array([np.real(c[:,0])**2+np.imag(c[:,0])**2, np.real(c[:,1])**2+np.imag(c[:,1])**2, np.real(c[:,2])**2+np.imag(c[:,2])**2],dtype=complex)

################LØSNINGEN MED HAMILTON#####################

Hamiltonian_Psi = np.zeros((3,len(t)),dtype=complex)
for i,tt in enumerate(t):
	Hamiltonian_Psi[0,i] = omega2(tt)/np.sqrt(omega1(tt)**2+omega2(tt)**2)
	Hamiltonian_Psi[1,i] = 0
	Hamiltonian_Psi[2,i] = -omega1(tt)/np.sqrt(omega1(tt)**2+omega2(tt)**2)
	
Hamiltonian_Psi_normkv = np.array([Hamiltonian_Psi[0]**2,Hamiltonian_Psi[1]**2,Hamiltonian_Psi[2]**2],dtype=complex)


##############FEJL############
inner_product = np.zeros((len(t),2),dtype=complex)
for i in range(0,len(t)):
	inner_product[i,0] = np.real(np.dot(Hamiltonian_Psi[:,i],Psi_udv[i,:]))
	inner_product[i,1] = np.imag(np.dot(Hamiltonian_Psi[:,i],Psi_udv[i,:]))
inner_product_norm = np.zeros(len(t),dtype=complex)
for i in range(0,len(t)):
	comp = inner_product[i,0]+inner_product[i,1]*1j
	conj = np.conjugate(comp)
	inner_product_norm[i] = comp*conj



################PLOTS###################
"""
fig,ax = plt.subplots(1,4,figsize = (20,4))

omega1s = [omega1(tt) for tt in t]
omega2s = [omega2(tt) for tt in t]

ax[0].plot(t,omega1s,'r',label = r'$\Omega_1$')
ax[0].plot(t,omega2s,'b',label = r'$\Omega_2$')
ax[0].set_xlabel('t')
ax[0].set_title('Koblingerne')
ax[0].legend()

ax[1].plot(t,Psi_udv_normkv[0],'r-',label = r'$|c_1|²$')
ax[1].plot(t,Psi_udv_normkv[1],'b-',label = r'$|c_2|²$')
ax[1].plot(t,Psi_udv_normkv[2],'g-',label = r'$|c_3|²$')
ax[1].legend()
ax[1].set_title('Løsning med complex ode')
ax[1].set_xlabel('t')
ax[1].grid(True)
"""
fig,ax = plt.subplots(1,1,figsize=(6,3))
ax.plot(t,Hamiltonian_Psi_normkv[0],'r-',label = r'$|c_1|²$')
ax.plot(t,Hamiltonian_Psi_normkv[1],'b-',label = r'$|c_2|²$')
ax.plot(t,Hamiltonian_Psi_normkv[2],'g-',label = r'$|c_3|²$')
#ax.set_title(r'Analytical solution')
ax.set_xlabel('t')
ax.grid(True)
ax.legend()
ax.set_xticklabels([])

xl = ax.get_xticks()
xl = ['' for x in xl]
xl[1] = '0'; xl[-2] = 'T'
ax.set_xticklabels(xl)

yl = ax.get_yticks()
yl = ['' for y in yl]
yl[1] = '0'; yl[-2] = '1'
ax.set_yticklabels(yl)


"""
ax[3].plot(t,inner_product_norm)
ax[3].set_title('Fidelity')
ax[3].set_xlabel('t')
ax[3].grid(True)
ax[3].ticklabel_format(useOffset=False)
"""

fig.tight_layout()
fig.savefig('analyticalSolution.pdf')

