import numpy as np
import scipy.optimize

def phi(n,var): 
	return abs(np.cos(var[0])/np.cos(var[1])-var[2]*n*np.sin(var[1])**(n-1))
	
N = 1.5
n = 2.

find_phi = lambda var: phi(n,var)

p = scipy.optimize.minimize(find_phi,[1,0.15,12])

print(p)

