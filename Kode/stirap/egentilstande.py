import numpy as np
import matplotlib.pyplot as plt
from omegaberegner import make_omega
"""
def omega2(t):
	if t <= 0:
		return 0
	elif t >= np.pi*N:
		return 0
	else: 
		return np.sin(t/N)**M

def omega1(t):
	if t <= N:
		return 0
	elif t >= T:
		return 0
	else:
		return np.sin((t-N)/N)**M
"""
def omega_rms(t):
	return np.sqrt(omega1(t)**2+omega2(t)**2)

	
M = 3.
T = 500
t = np.linspace(0,T,1000)
t_range = [t[0],t[-1]]
offset = 150
omega1,omega2 = make_omega(t_range,offset,M)


dark = np.zeros((3,len(t)),dtype=complex)
for i,tt in enumerate(t):
	dark[0,i] = omega2(tt)/omega_rms(tt)
	dark[1,i] = 0
	dark[2,i] = -omega1(tt)/omega_rms(tt)
dark_normkv = np.array([dark[0]**2,dark[1]**2,dark[2]**2])

excited = np.zeros((3,len(t)),dtype=complex)
for i,tt in enumerate(t):
	excited[0,i] = 0
	excited[1,i] = 1
	excited[2,i] = 0

bright = np.zeros((3,len(t)),dtype=complex) ###check normering
for i,tt in enumerate(t):
	bright[0,i] = omega1(tt)/omega_rms(tt)
	bright[1,i] = 0
	bright[2,i] = omega2(tt)/omega_rms(tt)

plus = (bright+excited)
plus_normkv = 1/np.sqrt(2)*np.array([plus[0]**2,plus[1]**2,plus[2]**2])
minus = (bright-excited)
minus_normkv = 1/np.sqrt(2)*np.array([minus[0]**2,minus[1]**2,minus[2]**2])


eps_0 = [0*tt for tt in t] 
eps_p = [omega_rms(tt) for tt in t]
eps_m = [-omega_rms(tt) for tt in t]


fig,ax = plt.subplots(1,1,figsize=(6,3))

ax.plot(t,eps_0,'k',label='Eigenvalue 0')
ax.plot(t,eps_p,'r',label=r'Eigenvalue $\sqrt{\Omega_1^2+\Omega_2^2}$')
ax.plot(t,eps_m,'b',label=r'Eigenvalue $-\sqrt{\Omega_1^2+\Omega_2^2}$')

ax.set_xlabel('t')
#ax.set_title('Eigenvalues of the STIRAP system')
ax.grid(True)
ax.legend()

xl = ax.get_xticks()
xl = ['' for x in xl]
xl[1] = '0'; xl[-2] = 'T'
ax.set_xticklabels(xl)

yl = ax.get_yticks()
middle = np.where(yl == 0)
yl = ['' for y in yl]
yl[1] = '-1'; yl[middle[0][0]] = '0'; yl[-2] = '1'
ax.set_yticklabels(yl)

fig.tight_layout()
fig.savefig('Egenvaerdier.pdf')


