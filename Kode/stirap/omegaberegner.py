import numpy as np
import scipy.optimize
import matplotlib.pyplot as plt

def sin2(t,phi):
    # Normal sine-function with phase-offset
    f = np.sin(t+phi)
    return f

def sin1(t,n,delta,a):
    # Sine-function to the power of n, with phase-offset and scale-value
    f = a*np.sin(t+delta)**n
    return f

def grad(n,phi,delta,a):
    # Function for difference in gradients of sin1 and sin2
    f = np.cos(phi)/np.cos(delta)-a*n*np.sin(delta)**(n-1)
    return abs(f)

def find_vars(var,n):
    # Sum of height and gradient difference, should be zeroed to match
    # Set to meet at t = 0, so the phases can line up
    height_diff = abs(sin2(0,var[0])-sin1(0,n,var[1],var[2])) 
    gradient = grad(n,var[0],var[1],var[2])
    return height_diff + gradient

def omega(t,timestamps,phases,scale,offset,power):
    # Scale the time values, and make it as array
    t = (np.asarray(t)-offset)/scale
    
    # Determine time intervals
    time1 = t[t <= timestamps[0]]
    time2 = t[np.logical_and(t <= timestamps[1], t > timestamps[0])]
    time3 = t[np.logical_and(t <= timestamps[2], t > timestamps[1])]
    time4 = t[np.logical_and(t <= timestamps[3], t > timestamps[2])]
    time5 = t[t > timestamps[3]]

    # Determine the output of the function in each interval
    out = np.zeros(len(time1))
    out = np.append(out, sin1(time2, power, phases[0][0], phases[0][1]))
    out = np.append(out, sin2(time3, phases[1][0]))
    out = np.append(out, sin1(time4, power, phases[2][0], phases[2][1]))
    out = np.append(out, np.zeros(len(time5)))
    return out

def make_omega(trange,offset,power):
    # Make function for minimization only dependent on phases (var)
    func = lambda var: find_vars(var,power)
    
    # Method for minimization, and the minimization with clever guesses for phases
    meth = 'Nelder-Mead'
    res1 = scipy.optimize.minimize(func,[np.pi/4,np.pi/4,1],method=meth)
    res2 = scipy.optimize.minimize(func,[3*np.pi/4,3*np.pi/4,1],method=meth)

    # The time intervals for determining which function to use where
    timestamps = [0, res1.x[1], res1.x[1]-res1.x[0]+res2.x[0], 
                  res1.x[1]-res1.x[0]+res2.x[0]+(np.pi-res2.x[1])]
    
    # Collecting the relevant phases for the functions
    phases = [[0, res1.x[2]], [res1.x[0] - res1.x[1]], 
              [res2.x[1] - timestamps[2], res2.x[2]]]

    # Determining the scale of the time
    scale = (trange[1]-trange[0]-offset)/(timestamps[-1])

    # Create the functions, so they are only a function of time
    om1 = lambda t: omega(t, timestamps, phases, scale, offset, power)
    om2 = lambda t: omega(t, timestamps, phases, scale, 0, power)
    
    # And done!
    return om1,om2

