import numpy as np
import matplotlib.pyplot as plt
from omegaberegner import make_omega

T = 500
t = np.linspace(0,T,1000)
t_range = [t[0], t[-1]]
n = 3 
offset = 150

omega1,omega2 = make_omega(t_range,offset,n)

Omega1,Omega2 = make_omega(t_range,offset,1)



fig,ax = plt.subplots(1,1,figsize=(6,3))

ax.plot(t,omega1(t),'r',label=r'$\Omega_1$')
ax.plot(t,omega2(t),'b',label=r'$\Omega_2$')

ax.plot(t,Omega1(t),'k--')
ax.plot(t,Omega2(t),'k--')


ax.grid(True)
ax.legend()
ax.set_xlabel(r'$t$')
ax.set_ylabel(r'$\Omega(t)$')
#ax.set_title(r'The coupling amplitudes $\Omega$')

xl = ax.get_xticks()
xl = ['' for x in xl]
xl[1] = '0'; xl[-2] = 'T'
ax.set_xticklabels(xl)

yl = ax.get_yticks()
yl = ['' for y in yl]
yl[1] = '0'; yl[-2] = '1'
ax.set_yticklabels(yl)

fig.tight_layout()
fig.savefig('Omegas.pdf')
