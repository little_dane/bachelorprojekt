import scipy.integrate
import numpy as np
import matplotlib.pyplot as plt
import cmath
from omegaberegner import make_omega
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)

################FUNKTIONER#################

def H(t):
    H = np.array([[0, omega1(t), 0], [omega1(t), 0, omega2(t)], [0, omega2(t), 0]],dtype=complex)
    return H

def dPsidt(t,Psi):
    dPsidt = 1/(1j*h_bar)*np.dot(H(t),Psi)
    return dPsidt

###########LØSNINGEN AF SCHRÖDINGERS LIGNING###################
M = 2

#Definition af variable
h_bar = 1.
Psi_start = np.array([1,0,0],dtype=complex) #Måske en anden starttilstand

T = 5000
t = np.linspace(0,T,10000)
t_range = [t[0],t[-1]]
offset = 1500

fig,ax = plt.subplots(1,1)

#Ved at bruge omegaberegner fås koblingerne
omega1,omega2 = make_omega(t_range,offset,M)


#For at få complex_ode til at virke, skal man sætte konstanten først
diff_eq = lambda t,y: dPsidt(t,y)

#Nu kan complex_ode kaldes og Hamiltonen udvikles
Psi_solve = scipy.integrate.complex_ode(diff_eq)
Psi_solve.set_initial_value(Psi_start,t[0])
Psi_solve.set_integrator('dop853')

#Først tilføjes det 0'te element, fordi dette udregnes før for-loopet
c = np.zeros((len(t),3),dtype=complex)
c[0,0] = Psi_start[0]
c[0,1] = Psi_start[1]
c[0,2] = Psi_start[2]


#Nu samles resterende data i listerne
for i,t_int in enumerate(t[1:]):
	out = Psi_solve.integrate(t_int)
	c[i+1,0] = out[0]
	c[i+1,1] = out[1]
	c[i+1,2] = out[2]

#Den udviklede bølgefunktion
Psi_udv = c

#Nedenstående vektor består af normkvadraterne af c1 og c2
Psi_udv_normkv = np.array([np.real(c[:,0])**2+np.imag(c[:,0])**2, np.real(c[:,1])**2+np.imag(c[:,1])**2, np.real(c[:,2])**2+np.imag(c[:,2])**2],dtype=complex)

################LØSNINGEN MED HAMILTON#####################
Hamiltonian_Psi = np.zeros((3,len(t)),dtype=complex)
for i,tt in enumerate(t):
	Hamiltonian_Psi[0,i] = omega2(tt)/np.sqrt(omega1(tt)**2+omega2(tt)**2)
	Hamiltonian_Psi[1,i] = 0
	Hamiltonian_Psi[2,i] = -omega1(tt)/np.sqrt(omega1(tt)**2+omega2(tt)**2)
	Hamiltonian_Psi_normkv = np.array([Hamiltonian_Psi[0]**2,Hamiltonian_Psi[1]**2,Hamiltonian_Psi[2]**2],dtype=complex)


u = - Psi_udv[:,2]
v = - Psi_udv[:,1]*1j
w = Psi_udv[:,0]
B = np.array([u,v,w])

angle = np.linspace(np.pi,3*np.pi,int(1e5))
cirkel = np.asarray([np.cos(angle),np.sin(angle),np.zeros(len(angle))])
X = np.real(u); Y = np.real(v); Z = np.real(w)

def lim_coords(cx,cy,cz,lx,ly,lz):
	wx = np.logical_and(cx>=lx[0],cx<=lx[1])
	wy = np.logical_and(cy>=ly[0],cy<=ly[1])
	wz = np.logical_and(cz>=lz[0],cz<=lz[1])
	w = np.where(np.logical_and(wx,np.logical_and(wy,wz)))[0]
	return cx[w],cy[w],cz[w]

fig = plt.figure(figsize=(8,6))
ax1 = fig.add_subplot(111,projection='3d')
ax1.scatter(X,Y,Z,c='r',s=0.1)
ax1.plot(cirkel[0],cirkel[1],cirkel[2],'--k')
ax1.plot(cirkel[2],cirkel[0],cirkel[1],'-k')

arrow1 = Arrow3D([0, X[-1]], [0, Y[-1]], [0, Z[-1]],
		mutation_scale=20, lw=3, arrowstyle='-|>',
		color='b')
ax1.add_artist(arrow1)

ax1.set_xticklabels([])
ax1.set_yticklabels([])
ax1.set_zticklabels([])

"""
ax2 = fig.add_subplot(122,projection='3d')
ax2.scatter(X,Y,Z,c='r',s=0.1)
lim = lambda val: 0. if abs(val)<0.01 else val
xlim = [lim(X.min()), lim(X.max())];
ylim = [lim(Y.min()), lim(Y.max())];
zlim = [lim(Z.min()), lim(Z.max())];

ax2.set_xlim(xlim)
ax2.set_ylim(ylim)
ax2.set_zlim(zlim)

c1x,c1y,c1z = lim_coords(cirkel[0],cirkel[1],cirkel[2],xlim,ylim,zlim)
c2x,c2y,c2z = lim_coords(cirkel[2],cirkel[0],cirkel[1],xlim,ylim,zlim)
ax2.plot(c1x,c1y,c1z,'--k')
ax2.plot(c2x,c2y,c2z,'-k')
arrow2 = Arrow3D([0, X[-1]], [0, Y[-1]], [0, Z[-1]],
		mutation_scale=20, lw=3, arrowstyle='-|>',
		color='b')
ax2.add_artist(arrow2)

ax2.set_xticklabels([])
ax2.set_yticklabels([])
ax2.set_zticklabels([])
"""

#fig.suptitle('Bloch sphere of the STIRAP system')
fig.tight_layout()
fig.savefig('Bloch.pdf')

 
