import scipy.integrate
import numpy as np
import matplotlib.pyplot as plt
import cmath
from omegaberegner import make_omega

################FUNKTIONER#################

def H(t):
    H = np.array([[0, omega1(t), 0], [omega1(t), 0, omega2(t)], [0, omega2(t), 0]],dtype=complex)
    return H

def dPsidt(t,Psi):
    dPsidt = 1/(1j*h_bar)*np.dot(H(t),Psi)
    return dPsidt
"""
def omega2(t,N):
	if t <= 0:
		return 0
	elif t >= np.pi*N:
		return 0
	else:
		return np.sin(t/N)**M

def omega1(t,N):
	if t <= N:
		return 0
	elif t >= T:
		return 0
	else:
		return np.sin((t-N)/N)**M
"""
###########LØSNINGEN AF SCHRÖDINGERS LIGNING###################
N_values = [1, 10, 25, 50, 100, 500] 
#N_values = [100, 200, 300, 400, 500, 600] 
offset_values = [0.3, 3, 7.5, 15, 30, 150]
#offset_values = [30, 60, 90, 120, 150, 180]



#Definition af variable0
h_bar = 1.
Psi_start = np.array([1,0,0],dtype=complex) #Måske en anden starttilstand

M = 3 

#Definerer plottet
fig = plt.figure(figsize = (7,6))
figspec = fig.add_gridspec(3,4)
fidelity_axis = fig.add_subplot(figspec[:,-2:])
axis=[]
for i in range(2):
	for j in range(3):
		axis.append(fig.add_subplot(figspec[j,i]))



for w,(N,offset) in enumerate(zip(N_values,offset_values)):
	t = np.linspace(0,N,1000)
	t_range = [t[0],t[-1]]
	omega1,omega2 = make_omega(t_range,offset,M)
#For at få complex_ode til at virke, skal man sætte konstanten først
	diff_eq = lambda t,y: dPsidt(t,y)

#Nu kan complex_ode kaldes og Hamiltonen udvikles
	Psi_solve = scipy.integrate.complex_ode(diff_eq)
	Psi_solve.set_initial_value(Psi_start,t[0])
	Psi_solve.set_integrator('dop853')

#Først tilføjes det 0'te element, fordi dette udregnes før for-loopet
	c = np.zeros((len(t),3),dtype=complex)
	c[0,0] = Psi_start[0]
	c[0,1] = Psi_start[1]
	c[0,2] = Psi_start[2]


#Nu samles resterende data i listerne
	for i,t_int in enumerate(t[1:]):
		out = Psi_solve.integrate(t_int)
		c[i+1,0] = out[0]
		c[i+1,1] = out[1]
		c[i+1,2] = out[2]

#Den udviklede bølgefunktion
	Psi_udv = c

#Nedenstående vektor består af normkvadraterne af c1 og c2
	Psi_udv_normkv = np.array([np.real(c[:,0])**2+np.imag(c[:,0])**2, np.real(c[:,1])**2+np.imag(c[:,1])**2, np.real(c[:,2])**2+np.imag(c[:,2])**2],dtype=complex)

################LØSNINGEN MED HAMILTON#####################
	Hamiltonian_Psi = np.zeros((3,len(t)),dtype=complex)
	for i,tt in enumerate(t):
		Hamiltonian_Psi[0,i] = omega2(tt)/np.sqrt(omega1(tt)**2+omega2(tt)**2)
		Hamiltonian_Psi[1,i] = 0
		Hamiltonian_Psi[2,i] = -omega1(tt)/np.sqrt(omega1(tt)**2+omega2(tt)**2)
	Hamiltonian_Psi_normkv = np.array([Hamiltonian_Psi[0]**2,Hamiltonian_Psi[1]**2,Hamiltonian_Psi[2]**2],dtype=complex)


##############FEJL############
	inner_product = np.zeros((len(t),2),dtype=complex)
	for i in range(0,len(t)):
		inner_product[i,0] = np.real(np.dot(Hamiltonian_Psi[:,i],Psi_udv[i,:]))
		inner_product[i,1] = np.imag(np.dot(Hamiltonian_Psi[:,i],Psi_udv[i,:]))
	inner_product_norm = np.zeros(len(t),dtype=complex)
	for i in range(0,len(t)):
		comp = inner_product[i,0]+inner_product[i,1]*1j
		conj = np.conjugate(comp)
		inner_product_norm[i] = comp*conj
	ts = [tt/t[-1] for tt in t]
	

	axis[w].plot(np.asarray(t)/N,Psi_udv_normkv[0],'r-',label=r'$|c_1|^2$')	
	axis[w].plot(np.asarray(t)/N,Psi_udv_normkv[1],'b-',label=r'$|c_2|^2$')
	axis[w].plot(np.asarray(t)/N,Psi_udv_normkv[2],'g-',label=r'$|c_3|^2$')
	axis[w].set_title(r'T = '+str(N))
	axis[w].set_xlabel(r'$t/T$')

	fidelity_axis.plot(ts,inner_product_norm,label=r'T = '+str(N))
	fidelity_axis.set_title('Fidelity')
	fidelity_axis.set_xlabel(r'$t/T$')
	fidelity_axis.grid(True)

for a in [axis[3], axis[4], axis[5]]:
	a.set_yticks([])
for a in [axis[0], axis[1], axis[3], axis[4]]:
	a.set_xticks([])
	a.set_xlabel('')
fig.tight_layout()
for ax in [axis[2], axis[5], fidelity_axis]:
	xl = ax.get_xticks()
	print(xl)
	xl = ['' for x in xl]
	xl[1] = '0'; xl[-2] = '1'
	ax.set_xticklabels(xl)
for ax in axis[:3]:
	yl = ax.get_yticks()
	yl = ['' for y in yl]
	yl[1] = '0'; yl[-2] = '1'
	ax.set_yticklabels(yl)


fidelity_axis.legend(fontsize=12)
fig.savefig('korttid.pdf')

