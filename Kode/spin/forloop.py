import scipy.integrate
import numpy as np
import matplotlib.pyplot as plt
import cmath

################FUNKTIONER#################

def H(theta):
    #Definition af funktionen for B-feltet
    B = B_0*np.array([np.sin(theta)*np.cos(phi),np.sin(phi)*np.cos(phi),np.cos(theta)])

    #Definition af spin-matricer
    S_x = h_bar/2*np.array([[0,1],[1,0]])
    S_y = h_bar/2*np.array([[0,-1j],[1j,0]])
    S_z = h_bar/2*np.array([[1,0],[0,-1]])
    S = np.array([S_x,S_y,S_z])

    #Definition af Hamilton
    H = -gamma*(B[0]*S[0]+B[1]*S[1]+B[2]*S[2])
    return H

def dPsidt(t,Psi,a):
    dPsidt = 1/(1j*h_bar)*np.dot(H(theta(t,N,Delta,T)),Psi)
    return dPsidt

def f(t,N,Delta,T):
	return np.pi/(2*Delta**N+(T-2*Delta)*N*Delta**(N-1))*t**N

def g(t,Delta,T):
	return f(Delta,N,Delta,T)+(t-Delta)/(T-2*Delta)*(np.pi-2*f(Delta,N,Delta,T))

def theta(t,N,Delta,T):
	if t <= 0:
		return 0
	elif t >= T:
		return np.pi 
	elif t > 0 and t <= Delta:
		return f(t,N,Delta,T)
	elif t < T and t > T-Delta:
		return np.pi-f(T-t,N,Delta,T)
	else:
		return g(t,Delta,T)

a_values = [5,1,0.5,0.1,0.05,0.01] 
a_values = [0.1,0.08,0.06,0.04,0.02,0.01]

N = 3

fig = plt.figure(figsize=(7,6))
figspec = fig.add_gridspec(3,4)
fidelity_axis = fig.add_subplot(figspec[:,-2:])
axis = []
for i in range(2):
	for j in range(3):
		axis.append(fig.add_subplot(figspec[j,i]))


for w,a in enumerate(a_values):
	###########LØSNINGEN AF SCHRÖDINGERS LIGNING###################

	#Definition af variable
	h_bar = 6.5821*10**(-15)
	B_0 = 1.
	gamma = 1.
	phi = 0.
	Psi_start = np.array([1,0])
	T = np.pi/a #t_max
	Delta = T/3
	t=np.linspace(0,T,1000) #Dette er tiden T. Theta er a*t

	#For at få complex_ode til at virke, skal man sætte konstanten først
	diff_eq = lambda t,y: dPsidt(t,y,a)

	#Nu kan complex_ode kaldes og Hamiltonen udvikles
	Psi_solve = scipy.integrate.complex_ode(diff_eq)
	Psi_solve.set_initial_value(Psi_start,t[0])
	#Der defineres to lister til c1 og c2 hhv.
	c1 = np.zeros((len(t),2))
	c2 = np.zeros((len(t),2))
	#Først tilføjes det 0'te element, fordi dette udregnes før for-loopet
	c1[0,0] = np.real(Psi_start[0])
	c1[0,1] = np.imag(Psi_start[0])
	c2[0,0] = np.real(Psi_start[1])
	c2[0,1] = np.imag(Psi_start[1])
	#Nu samles resterende data i listerne
	for i,t_int in enumerate(t[1:]):
	    out = Psi_solve.integrate(t_int)
	    c1[i+1,0] = np.real(out[0])
	    c1[i+1,1] = np.imag(out[0])
	    c2[i+1,0] = np.real(out[1])
	    c2[i+1,1] = np.imag(out[1])

	#Den udviklede bølgenfunktion
	Psi_udv = np.array([c1[:,0]+c1[:,1]*1j,c2[:,0]+c2[:,1]*1j])

	#Nedenstående vektor består af normkvadraterne af c1 og c2
	Psi_udv_normkv = np.array([c1[:,0]**2+c1[:,1]**2,c2[:,0]**2+c2[:,1]**2])

	################LØSNINGEN MED HAMILTON#####################
	Hamiltonian_Psi = np.zeros((2,len(t)))
	for i in range(0,len(t)):
		Hamiltonian_Psi[0,i] = np.cos(theta(t[i],N,Delta,T)/2)
		Hamiltonian_Psi[1,i] = np.sin(theta(t[i],N,Delta,T)/2)

	
	##############FEJL############
	inner_product = np.zeros((len(t),2))
	for i in range(0,len(t)):
		inner_product[i,0] = np.real(np.dot(Hamiltonian_Psi[:,i],Psi_udv[:,i]))
		inner_product[i,1] = np.imag(np.dot(Hamiltonian_Psi[:,i],Psi_udv[:,i]))
	inner_product_norm = np.zeros(len(t),dtype=complex)
	for i in range(0,len(t)):
		comp = inner_product[i,0]+inner_product[i,1]*1j
		conj = np.conjugate(comp)
		inner_product_norm[i] = comp*conj
	ts = [tt/t[-1] for tt in t]
	
	axis[w].plot(ts,Psi_udv_normkv[0],'r-',label=r'$|c_1|^2$')
	axis[w].plot(ts,Psi_udv_normkv[1],'b-',label=r'$|c_2|^2$')
	axis[w].set_title(r'a = '+str(a))
	axis[w].set_xlabel(r'$t/T$')

	fidelity_axis.plot(ts,inner_product_norm,label=r'a = ' + str(a))
	fidelity_axis.set_title('Fidelity')
	fidelity_axis.set_xlabel(r'$t/T$')
	#fidelity_axis.set_ylabel(r'$F$')
	fidelity_axis.grid(True)

for a in [axis[3], axis[4], axis[5]]:
	a.set_yticks([])
for a in [axis[0], axis[1], axis[3], axis[4]]:
	a.set_xticks([])
	a.set_xlabel('')
fig.tight_layout()
for ax in [axis[2], axis[5], fidelity_axis]:
	xl = ax.get_xticks()
	xl = ['' for x in xl]
	xl[1] = '0'; xl[-2] = '1'
	ax.set_xticklabels(xl)
for ax in axis[:3]:
	yl = ax.get_yticks()
	yl = ['' for y in yl]
	yl[1] = '0'; yl[-2] = '1'
	ax.set_yticklabels(yl)



fidelity_axis.legend(fontsize=12)
fig.savefig('megetlillea.pdf')
fig.tight_layout()
