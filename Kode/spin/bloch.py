import scipy.integrate
import numpy as np
import matplotlib.pyplot as plt
import cmath
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

class Arrow3D(FancyArrowPatch):
	def __init__(self, xs, ys, zs, *args, **kwargs):
		FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
		self._verts3d = xs, ys, zs
	
	def draw(self, renderer):
		xs3d, ys3d, zs3d = self._verts3d
		xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
		self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
		FancyArrowPatch.draw(self, renderer)

################FUNKTIONER#################

def H(theta):
    #Definition af funktionen for B-feltet
    B = B_0*np.array([np.sin(theta)*np.cos(phi),np.sin(phi)*np.cos(phi),np.cos(theta)])

    #Definition af spin-matricer
    S_x = h_bar/2*np.array([[0,1],[1,0]])
    S_y = h_bar/2*np.array([[0,-1j],[1j,0]])
    S_z = h_bar/2*np.array([[1,0],[0,-1]])
    S = np.array([S_x,S_y,S_z])

    #Definition af Hamilton
    H = -gamma*(B[0]*S[0]+B[1]*S[1]+B[2]*S[2])
    return H

def dPsidt(t,Psi,a):
    dPsidt = 1/(1j*h_bar)*np.dot(H(theta(t,N,Delta,T)),Psi)
    return dPsidt

def f(t,N,Delta,T):
	return np.pi/(2*Delta**N+(T-2*Delta)*N*Delta**(N-1))*t**N

def g(t,Delta,T):
	return f(Delta,N,Delta,T)+(t-Delta)/(T-2*Delta)*(np.pi-2*f(Delta,N,Delta,T))

def theta(t,N,Delta,T):
	if t <= 0:
		return 0
	elif t >= T:
		return np.pi 
	elif t > 0 and t <= Delta:
		return f(t,N,Delta,T)
	elif t < T and t > T-Delta:
		return np.pi-f(T-t,N,Delta,T)
	else:
		return g(t,Delta,T)

###########LØSNINGEN AF SCHRÖDINGERS LIGNING###################

#Definition af variable
h_bar = 1.
B_0 = 1.
gamma = 1.
phi = 0.
Psi_start = np.array([1,0])
a = 0.0005
N = 3
T=np.pi/a
Delta = T/3
t=np.linspace(0,T,1000)

#For at få complex_ode til at virke, skal man sætte konstanten først
diff_eq = lambda t,y: dPsidt(t,y,a)

#Nu kan complex_ode kaldes og Hamiltonen udvikles
Psi_solve = scipy.integrate.complex_ode(diff_eq)
Psi_solve.set_initial_value(Psi_start,t[0])
#Der defineres to lister til c1 og c2 hhv.
c1 = np.zeros((len(t),2))
c2 = np.zeros((len(t),2))
#Først tilføjes det 0'te element, fordi dette udregnes før for-loopet

c1[0,0] = np.real(Psi_start[0])
c1[0,1] = np.imag(Psi_start[0])
c2[0,0] = np.real(Psi_start[1])
c2[0,1] = np.imag(Psi_start[1])
#Nu samles resterende data i listerne
for i,t_int in enumerate(t[1:]):
    out = Psi_solve.integrate(t_int)
    c1[i+1,0] = np.real(out[0])
    c1[i+1,1] = np.imag(out[0])
    c2[i+1,0] = np.real(out[1])
    c2[i+1,1] = np.imag(out[1])

#Den udviklede bølgefunktion
Psi_udv = np.array([c1[:,0]+c1[:,1]*1j,c2[:,0]+c2[:,1]*1j])

#Nedenstående vektor består af normkvadraterne af c1 og c2
Psi_udv_normkv = np.array([c1[:,0]**2+c1[:,1]**2,c2[:,0]**2+c2[:,1]**2])

################LØSNINGEN MED HAMILTON#####################
Hamiltonian_Psi = np.zeros((2,len(t)))
for i in range(0,len(t)):
	Hamiltonian_Psi[0,i] = np.cos(theta(t[i],N,Delta,T)/2)
	Hamiltonian_Psi[1,i] = np.sin(theta(t[i],N,Delta,T)/2)
Hamiltonian_Psi_normkv = np.array([Hamiltonian_Psi[0]**2,Hamiltonian_Psi[1]**2])


##############FEJL############
inner_product = np.zeros((len(t),2))
for i in range(0,len(t)):
	inner_product[i,0] = np.real(np.dot(Hamiltonian_Psi[:,i],Psi_udv[:,i]))
	inner_product[i,1] = np.imag(np.dot(Hamiltonian_Psi[:,i],Psi_udv[:,i]))
inner_product_norm = np.zeros(len(t),dtype=complex)
for i in range(0,len(t)):
	comp = inner_product[i,0]+inner_product[i,1]*1j
	conj = np.conjugate(comp)
	inner_product_norm[i] = comp*conj

u = 2*np.real(Psi_udv[0,:]*np.conj(Psi_udv[1,:]))
v = 2*np.imag(Psi_udv[0,:]*np.conj(Psi_udv[1,:]))
w = abs(Psi_udv[0,:])**2-abs(Psi_udv[1,:])**2

angle = np.linspace(np.pi,3*np.pi,int(1e5))
cirkel = np.asarray([np.cos(angle),np.sin(angle),np.zeros(len(angle))])
X = np.real(u); Y = np.real(v); Z = np.real(w)

def lim_coords(cx,cy,cz,lx,ly,lz):
	wx = np.logical_and(cx>=lx[0],cx<=lx[1])
	wy = np.logical_and(cy>=ly[0],cy<=ly[1])
	wz = np.logical_and(cz>=lz[0],cz<=lz[1])
	w = np.where(np.logical_and(wx,np.logical_and(wy,wz)))[0]
	return cx[w],cy[w],cz[w]

################PLOTS###################
fig = plt.figure(figsize = (8,6))
ax1 = fig.add_subplot(projection = '3d')
ax1.scatter(X,Y,Z,c='r',s=0.1)
ax1.plot(cirkel[0],cirkel[1],cirkel[2],'--k')
ax1.plot(cirkel[2],cirkel[0],cirkel[1],'-k')

arrow1 = Arrow3D([0,X[-1]],[0,Y[-1]],[0,Z[-1]],mutation_scale=20,lw=3,arrowstyle='-|>',color='b')

ax1.add_artist(arrow1)

ax1.set_xticklabels([])
ax1.set_yticklabels([])
ax1.set_zticklabels([])

#ax1.set_title('Bloch sphere of the spin system')

"""
ax2 = fig.add_subplot(122,projection='3d')
ax2.scatter(X,Y,Z,c='r',s=0.1)

lim = lambda val: 0. if abs(val)<0.01 else val
xlim = [lim(X.min()),lim(X.max())];
ylim = [lim(Y.min()),lim(Y.max())];
zlim = [lim(Z.min()),lim(Z.max())];

ax2.set_xlim(xlim)
ax2.set_ylim(ylim)
ax2.set_zlim(zlim)

c1x,c1y,c1z = lim_coords(cirkel[0],cirkel[1],cirkel[2],xlim,ylim,zlim)
c2x,c2y,c2z = lim_coords(cirkel[2],cirkel[0],cirkel[1],xlim,ylim,zlim)
ax2.plot(c1x,c1y,c1z,'--k')
ax2.plot(c2x,c2y,c2z,'-k')
arrow2 = Arrow3D([0,X[-1]],[0,Y[-1]],[0,Z[-1]],mutation_scale=20,lw=3,arrowstyle='-|>',color='b')
ax2.add_artist(arrow2)
"""
fig.tight_layout()

fig.savefig('Bloch.pdf')

