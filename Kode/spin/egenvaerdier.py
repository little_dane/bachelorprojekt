import numpy as np
import matplotlib.pyplot as plt

t = np.linspace(0,200,1000)

val1 = np.ones(len(t))
val2 = -np.ones(len(t))
zero = np.zeros(len(t))

fig,ax = plt.subplots(1,1,figsize=(6,3))

ax.plot(t,val1,'r',label = 'Eigenvalue 1')
ax.plot(t,val2,'b',label = 'Eigenvalue -1')
ax.plot(t,zero,'k')

ax.grid(True)
ax.set_xlabel(r'$t$')
#ax.set_title('Eigenvalues of the spin system')
ax.legend(bbox_to_anchor=(0.96,0.96),loc='upper right')

xl = ax.get_xticks()
xl = ['' for x in xl]
xl[1] = '0'; xl[-2] = 'T'
ax.set_xticklabels(xl)

yl = ax.get_yticks()
middle = np.where(yl == 0)
yl = ['' for y in yl]
yl[1] = '-1'; yl[middle[0][0]] = '0' ; yl[-2] = '1'
ax.set_yticklabels(yl)

fig.tight_layout()
fig.savefig('Egenvaerdier.pdf')
