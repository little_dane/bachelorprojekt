import numpy as np
import matplotlib.pyplot as plt
def f(t,N,Delta,T):
	return 1/(2*Delta**N+(T-2*Delta)*N*Delta**(N-1))*t**N

def g(t,Delta,T):
	return f(Delta,N,Delta,T)+(t-Delta)/(T-2*Delta)*(1-2*f(Delta,N,Delta,T))

def theta(t,N,Delta,T):
	if t <= 0:
		return 0
	elif t >= T:
		return 1 
	elif t > 0 and t <= Delta:
		return f(t,N,Delta,T)
	elif t < T and t > T-Delta:
		return 1-f(T-t,N,Delta,T)
	else:
		return g(t,Delta,T)

def theta2(t,a):
	if t <= 0:
		return 0
	elif t >= T:
		return 1
	else:
		return a/np.pi*t

a = 0.5 
N = 5 
T = np.pi/a
Delta = T/3
t = np.linspace(-0.5,T+0.5,1000)

theta_plot = [theta(tt,N,Delta,T) for tt in t]
theta2s = [theta2(tt,a) for tt in t]

fig,ax = plt.subplots(1,1,figsize=(6,3))

ax.plot(np.asarray(t)/T,theta_plot, 'r--')
ax.plot(np.asarray(t)/T,theta2s,'b-')

ax.grid(True)
ax.set_xlabel(r'$t$')
ax.set_ylabel(r'$\theta(t)$')
ax.set_xlim(-0.05,1.05)
#ax.set_title(r'Designing $\theta$')

xl = ax.get_xticks()
xl = ['' for x in xl]
xl[1] = '0'; xl[-2] = 'T'
ax.set_xticklabels(xl)

yl = ax.get_yticks()
yl = ['' for y in yl]
yl[1] = '0'; yl[-2] = '1'
ax.set_yticklabels(yl)


fig.tight_layout()
fig.savefig('Theta.pdf')
