import scipy.integrate
import numpy as np
import matplotlib.pyplot as plt
import cmath
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

class Arrow3D(FancyArrowPatch):
	def __init__(self, xs, ys, zs, *args, **kwargs):
		FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
		self._verts3d = xs, ys, zs
	
	def draw(self, renderer):
		xs3d, ys3d, zs3d = self._verts3d
		xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
		self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
		FancyArrowPatch.draw(self, renderer)

################FUNKTIONER#################

#u = 
#v = 
#w = 

angle = np.linspace(np.pi,3*np.pi,int(1e5))
cirkel = np.asarray([np.cos(angle),np.sin(angle),np.zeros(len(angle))])
#X = np.real(u); Y = np.real(v); Z = np.real(w)

def lim_coords(cx,cy,cz,lx,ly,lz):
	wx = np.logical_and(cx>=lx[0],cx<=lx[1])
	wy = np.logical_and(cy>=ly[0],cy<=ly[1])
	wz = np.logical_and(cz>=lz[0],cz<=lz[1])
	w = np.where(np.logical_and(wx,np.logical_and(wy,wz)))[0]
	return cx[w],cy[w],cz[w]

################PLOTS###################
fig = plt.figure(figsize = (8,6))
ax1 = fig.add_subplot(projection = '3d')
#ax1.scatter(X,Y,Z,c='r',s=0.1)
ax1.plot(cirkel[0],cirkel[1],cirkel[2],'--k')
ax1.plot(cirkel[2],cirkel[0],cirkel[1],'-k')

arrow1 = Arrow3D([0,1],[0,0],[0,0],mutation_scale=20,lw=3,arrowstyle='-|>',color='b')
ax1.add_artist(arrow1)
arrow2 = Arrow3D([0,0],[0,1],[0,0],mutation_scale=20,lw=3,arrowstyle='-|>',color='r')
ax1.add_artist(arrow2)
arrow3 = Arrow3D([0,0],[0,0],[0,1],mutation_scale=20,lw=3,arrowstyle='-|>',color='g')
ax1.add_artist(arrow3)

ax1.set_xticklabels([])
ax1.set_yticklabels([])
ax1.set_zticklabels([])

#ax1.set_title('Bloch sphere')
ax1.legend((arrow1,arrow2,arrow3),(r'$\hat{e}_1$',r'$\hat{e}_2$',r'$\hat{e}_3$'),fontsize=14)

fig.tight_layout()

fig.savefig('Bloch.pdf')

